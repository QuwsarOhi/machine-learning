import numpy as np
from matplotlib import pyplot as plt
import pylab as pl

class Preceptron():
	
	def __init__(self, eta = 0.01, number_of_iteration = 10):
		self.eta = eta
		self.number_of_iteration = 10
		
	def fit(self, X, y):
		self.w = np.zeros(X.shape[1] + 1)
		
		for iteration_no in range(self.number_of_iteration):
			for xi, target_lable in zip(X, y):
				error = (target_lable - self.predict(xi))
				update = self.eta * error		# eta is the ratio of update, if it is huge, the update will be huge also
				self.w[0] = update
				self.w[1:] += update * xi

	def predict(self, X):
		dot_product = np.dot(X, self.w[1:]) + self.w[0]
		return np.where(dot_product >= 0, 1, -1)



training_data = np.array([[0, 0, 1], [1, 1, 1], [1, 0, 1], [0, 1, 1]])
training_lable = np.array([0, 1, 1, 0])
test_data = np.array([1, 0, 0])
test_lable = np.array([1])


per = Preceptron()
per.fit(training_data, training_lable)
z = per.predict(test_data)
print(z)
