import numpy as np
from matplotlib import pyplot as plt
import pylab as pl

class PrettyPicture():
	
	def __init__(self, x_min, x_max, y_min, y_max, classifier, min_step = 0.01):
		self.x_min = x_min
		self.x_max = x_max
		self.y_min = y_min
		self.y_max = y_max
		self.min_step = min_step
		self.plt = plt
		self.classifier = classifier
		self.markers = ('x', 'o', '^', 'v')
		self.colors = ('blue', 'red', 'lightgreen', 'gray')
		self.no = 0
		self.plt.xlim(x_min, x_max)
		self.plt.ylim(y_min, y_max)
	
	# Use Scatter after ColorSpread
	def Scatter(self, X, y):
		#self.plt.scatter(x, y, marker = self.markers[self.no], color = self.colors[self.no])
		#self.no+=1
		for idx, cl in enumerate(np.unique(y)):
			plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1], marker=self.markers[idx], label=cl)

		
	def ColorSpread(self):
		# meshgrid returns two matrix where xx contains rows where x(i, 1) - x(i, 0) = 0.01, and same column element
		# yy contains rows where y(1, j) - y(0, j) = 0.01, and same row element
		# np.arrange(start, end, d) builds an one row array where x[i+1] - x[i] = d
		xx, yy = np.meshgrid(np.arange(self.x_min, self.x_max, self.min_step), np.arange(self.y_min, self.y_max, self.min_step))
		# ravel() returns one row matrix
		# np.c_(xx, yy) makes i rows by taking x(i) and y(i) element where xx and yy are one row matrix
		# so the shape of np_(xx, yy) = [size_of_xx_or_yy, 2]
		Z = self.classifier.predict(np.c_[xx.ravel(), yy.ravel()])
		# Colormesh
		# Z.reshape(xx.shape) shapes Z according to the shape of xx if the total element of Z and xx is same
		Z = Z.reshape(xx.shape)
		
		# USE ANY ONE OF THESE TWO
		# self.plt.pcolormesh(xx, yy, Z, alpha = 0.8, cmap = pl.cm.seismic)		# cmap is optional
		plt.contourf(xx, yy, Z, alpha=0.4, cmap = pl.cm.seismic)
	
	# Use Save before calling Show
	
	def Save(self, file_name):
		self.plt.savefig(file_name)
	
	def Lable(self, x, y):
		self.plt.xlable(x)
		self.plt.ylable(y)
	
	
	def Show(self):
		plt.legend(loc = 'upper left')
		self.plt.show()




'''
# MeshGrid code start
# Setting the boundaries of x and y
x_min = 0; x_max = 9
y_min = 0; y_max = 10

# This is the difference for each step calculation
h = .01

# meshgrid returns two matrix where xx contains rows where x(i, 1) - x(i, 0) = 0.01, and same column element
# yy contains rows where y(1, j) - y(0, j) = 0.01, and same row element
# np.arrange(start, end, d) builds an one row array where x[i+1] - x[i] = d
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))


# ravel() returns one row matrix
# np.c_(xx, yy) makes i rows by taking x(i) and y(i) element where xx and yy are one row matrix
# so the shape of np_(xx, yy) = [size_of_xx_or_yy, 2]
Z = classifier.predict(np.c_[xx.ravel(), yy.ravel()])


# Colormesh
# Z.reshape(xx.shape) shapes Z according to the shape of xx if the total element of Z and xx is same
Z = Z.reshape(xx.shape)
plt.xlim(xx.min(), xx.max())
plt.ylim(yy.min(), yy.max())
plt.pcolormesh(xx, yy, Z, cmap = pl.cm.seismic)		# cmap is optional

# Scattering the test_datas
# here x[:3, 0] means an array of: first 3 rows of x with column zero
# x[3:, 1] means an array of: rows after 3 with columns one
plt.scatter(training_data[:3, 0], training_data[:3, 1], marker = 'o', color = 'green')
plt.scatter(training_data[3:, 0], training_data[3:, 1], marker = 'o', color = 'red')

# Scattering the predicted datas according to their prediction
for i in range(0, len(prediction)):
	if prediction[i] == 1:
		plt.scatter(test_data[i][0], test_data[i][1], marker = 'x', color = 'green')
	else:
		plt.scatter(test_data[i][0], test_data[i][1], marker = 'x', color = 'red')

# show the plots
plt.show()

'''
