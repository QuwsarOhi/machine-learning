import numpy as np
from matplotlib import pyplot as plt
from sklearn.naive_bayes import GaussianNB
import pylab as pl
from sklearn.metrics import accuracy_score
from prettypicture import PrettyPicture




# Training Data
training_data = np.array([[1, 1], [1, 3], [3, 1], [7, 7], [5, 7], [7, 5]])
target = np.array([1, 1, 1, 2, 2, 2])

# Test data
test_data = np.array([[.9, .7], [8, 8]])
test_target = np.array([1, 2])

# Creating classifier, and predicting
classifier = GaussianNB()
classifier.fit(training_data, target)
prediction = classifier.predict(test_data)

# Calculating the accuracy
print('Accuracy ot the test : %.2f' %accuracy_score(test_target, prediction))

pic = PrettyPicture(0, 10, 0, 10, classifier)
pic.ColorSpread()
pic.Scatter(training_data, target)
pic.Scatter(test_data, prediction)
pic.Save('GaussianNB.png')
pic.Show()

