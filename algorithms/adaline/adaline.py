import numpy as np

# Adaptive Linear Neuron (ADALINE)

class Adaline(object):
	"""ADApive LInear NEuron classifier
	
	Parameters:
	----------------
	eta : float
		Learning rate (between 0.0 and 1.0)
	n_iter : int
		Passes over the training dataset.
		
	Attributes
	----------------
	w_ : 1d-array
		Weights after fitting.
	errors_ : list
		Number of misclassifications in every epoch.
	"""
	
	def __init__(self, eta=0.01, n_iter=50):
		self.eta = eta
		self.n_iter = n_iter
	
	def fit(self, X, y):
		""" Fit training data.
		Parameters
		----------
		X : {array-like}, shape = [n_samples, n_features]
			Training vectors,
			where n_samples is the number of samples and
			n_features is the number of features.
		y : array-like, shape = [n_samples]
			Target values.
		Returns
		-------
		self : object
		"""
		
		self.w_ = np.zeros(1 + X.shape[1])
		self.cost_ = []
	
		for i in range(self.n_iter):
			output = self.net_input(X)		
			# output is the list of dot product
			# [ X(1) dot w, X(2) dot w, X(3) dot w ...... X(n) dot w]
			errors = (y - output)
			# errors is the list of difference for every y and X(i) dot w 
			# length of errors = n_samples
			# now update the weights
			# for w[1:] = dot product of errors and transpose of X
			# where X shape = [n_features, n_samples]
			self.w_[1:] += self.eta * X.T.dot(errors)
			self.w[0] += self.eta * errors.sum()
			cost = (errors**2).sum() / 2.0
			self.cost_.append(cost)
		return self
	
	def net_input(self, X):
		""" Calculate net input"""
		return np.dot(X, self.w_[1:] + self.w_[0])
	
	def activation(self, X):
		""" Compute linear activation """
		return self.net_input(X)
		
	def predict(self, X):
		""" Return class label after unit step """
		return np.where(self.activation(X) >= 0.0, 1, -1)
		

