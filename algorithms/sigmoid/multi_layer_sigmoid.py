import numpy as np

class NeuronLayer():
	
	# Number of inputs, Number of neurons
	# Building a matrix of shape (number_of_inputs_per_neuron, number_of_neurons)
	def __init__(self, number_of_neurons, number_of_inputs_per_neuron):
		#self.w = np.array( [ np.zeros(number_of_neurons), ] * number_of_inputs_per_neuron)
		self.w = 2 * np.random.random((number_of_inputs_per_neuron, number_of_neurons)) - 1
		print(self.w)

class NeuralNet():
	
	def __init__(self, layer1, layer2, n_iter = 1000):
		self.layer1 = layer1
		self.layer2 = layer2
		self.n_iter = n_iter
	
	# Sigmoid is a S shaped curve
	# This normalizes the answer between 0 and 1	
	def sigmoid(self, X):
		return 1 / (1 + np.exp(-X))
	
	# Sigmoid derivative is the gradient of the sigmoid curve
	# It indicates how confident we are about the existing weight	
	def sigmoid_derivative(self, X):
		return X * (1 - X)
	
	def fit(self, X, y):
		for iteration in range(self.n_iter):
			# Pass the training set through our neural networ
			output_from_layer1 , output_from_layer2 = self.predict(X)
			print('output_from_layer1 shape : ', output_from_layer1.shape)
			print('output_from_layer2 shape : ', output_from_layer2.shape)
			# Calculate the error for layer 2 (The difference between the desired output
			# and the predicted output).
			
			print('y shape : ', y.shape)
			layer2_error = y - output_from_layer2
			print('layer2_error : ', layer2_error.shape)
			layer2_delta = layer2_error * self.sigmoid_derivative(output_from_layer2)
			print('layer2_delta shape : ', layer2_delta.shape)
			
			# Calculate the error for layer 1 (By looking at the weights in layer 1,
			# we can determine by how much layer 1 contributed to the error in layer 2).
			layer1_error = np.dot(layer2_delta, self.layer2.w.T)
			layer1_delta = layer1_error * self.sigmoid_derivative(output_from_layer1)
			
			layer1_adjustment = np.dot(X.T, layer1_delta)
			layer2_adjustment = np.dot(output_from_layer1.T, layer2_delta)
			
			self.layer1.w += layer1_adjustment
			self.layer2.w += layer2_adjustment
			
	def predict(self, X):
		output_from_layer1 = self.sigmoid(np.dot(X, self.layer1.w))
		output_from_layer2 = self.sigmoid(np.dot(output_from_layer1, self.layer2.w))
		return output_from_layer1, output_from_layer2
	
	def print_weights(self):
		print ("    Layer 1 (4 neurons, each with 3 inputs): ")
		print (self.layer1.w)
		print ("    Layer 2 (1 neuron, with 4 inputs):")
		print (self.layer2.w)


# 4 Neurons with 3 inputs in each Neuron
layer1 = NeuronLayer(4, 3)
# 1 Neuron with 4 inputs
layer2 = NeuronLayer(1, 4)

ANN = NeuralNet(layer1, layer2, 60000)

training_set_inputs = np.array([[0, 0, 1], [0, 1, 1], [1, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1], [0, 0, 0]])
training_set_outputs = np.array([[0, 1, 1, 1, 1, 0, 0]])

ANN.fit(training_set_inputs, training_set_outputs.T)

print('Synaptic Weights After Training:: ')
ANN.print_weights()

hidden, output = ANN.predict(np.array([1, 1, 0]))

print('Real output : ', output)
print('Hidden output : ', hidden)

