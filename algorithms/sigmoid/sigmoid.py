import numpy as np


class Sigmoid():
	
	def __init__(self, number_of_iteration = 10000):
		self.number_of_iteration = number_of_iteration
	
		
	def fit(self, X, y):
		self.w = np.zeros(X.shape[1])
		print('Shape : ', self.w.shape)
		for iteration in range(self.number_of_iteration):
			self.prediction = self.predict(X)
			error = y - self.prediction
			# print('prediction : ', prediction)
			print('error : ', error)
			print('sigmoid derivative : ', self.sigmoid_derivative(self.prediction))
			adjustment = np.dot(X.T, error * self.sigmoid_derivative(self.prediction))
			self.w += adjustment
	
	
	def sigmoid_derivative(self, X):
		return X * (1 - X)
		
	# This is the Sigmoid Function
	# It keeps the range of the output between 0 and 1
	def sigmoid(self, X):
		return 1 / (1 + np.exp(-X))
	
	
	def predict(self, X):
		dot_product = np.dot(X, self.w)
		# print(dot_product)
		return self.sigmoid(dot_product)
		
	def output_training_prediction(self):
		return self.prediction
		
	def weight(self):
		return self.w

training_data = np.array([[0, 0, 1], [1, 1, 1], [1, 0, 1], [0, 1, 1]])
training_output = np.array([[0, 1, 1, 1, 1, 0, 0]]).T

sigmoid = Sigmoid()
sigmoid.fit(training_data, training_output)
print(sigmoid.predict(np.array([1, 0, 0])))
print(sigmoid.output_training_prediction())
print(sigmoid.weight())
