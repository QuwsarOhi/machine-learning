#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###
from sklearn.svm import SVC
from class_vis import prettyPicture
from sklearn.metrics import accuracy_score

# Slicing and keeping only 1% of the actual features & labels
# features_train = features_train[:len(features_train)/100]
# labels_train = labels_train[:len(labels_train)/100]

clf = SVC(kernel='rbf', C=10000)
t0 = time()
clf.fit(features_train, labels_train)
print 'Training time:', round(time()-t0, 3), 's'
# prettyPicture(clf, features_train, labels_train)

t1 = time()
predicts = clf.predict(features_test)
print 'Predicting time:', round(time()-t1, 3), 's'
acc = accuracy_score(predicts, labels_test)

print 'Accuracy %.2f' %acc

chris = 0
for i in range(0, len(predicts)):
	if predicts[i] == 1:
		chris += 1

print 'Total(%d) written by Chris: %d' %(len(predicts), chris)

''' --------------------RESULTS--------------------

kernel = linear, C = auto, gamma = auto
test data = 50%
Training time: 104.858 s
Predicting time: 106.178 s
Accuracy 0.98

kernel = linear, C = auto, gamma = auto
test data = 1%
Training time: 0.039 s
Predicting time: 3.919 s
Accuracy 0.88

-----changing C------

kernel = rbf, C = auto, gamma = auto
test data = 1%
Training time: 0.162 s
Predicting time: 1.785 s
Accuracy 0.62

kernel = rbf, C = 10, gamma = auto
test data = 1%
Training time: 0.159 s
Predicting time: 1.8 s
Accuracy 0.62

kernel = rbf, C = 100, gamma = auto
test data = 1%
Training time: 0.164 s
Predicting time: 1.834 s
Accuracy 0.62

kernel = rbf, C = 1000, gamma = auto
test data = 1%
Training time: 0.164 s
Predicting time: 1.726 s
Accuracy 0.82

kernel = rbf, C = 10000, gamma = auto
test data = 1%
Training time: 0.153 s
Predicting time: 1.462 s
Accuracy 0.89

kernel = rbf, C = 100000, gamma = auto
test data = 1%
Training time: 0.151 s
Predicting time: 1.314 s
Accuracy 0.86

kernel = rbf, C = 10000, gamma = auto
test data = 50%
Training time: 146.011 s
Predicting time: 17.024 s
Accuracy 0.99

------------------


'''
