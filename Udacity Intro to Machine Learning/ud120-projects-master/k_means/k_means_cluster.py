#!/usr/bin/python 

""" 
    Skeleton code for k-means clustering mini-project.
"""




import pickle
import numpy
import matplotlib.pyplot as plt
import sys
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit




def Draw(pred, features, poi, mark_poi=False, name="image.png", f1_name="feature 1", f2_name="feature 2"):
    """ some plotting code designed to help you visualize your clusters """

    ### plot each cluster with a different color--add more colors for
    ### drawing more than five clusters
    colors = ["b", "c", "k", "m", "g"]
    for ii, pp in enumerate(pred):
        plt.scatter(features[ii][0], features[ii][1], color = colors[pred[ii]])

    ### if you like, place red stars over points that are POIs (just for funsies)
    if mark_poi:
        for ii, pp in enumerate(pred):
            if poi[ii]:
                plt.scatter(features[ii][0], features[ii][1], color="r", marker="*")
    plt.xlabel(f1_name)
    plt.ylabel(f2_name)
    plt.savefig(name)
    plt.show()



### load in the dict of dicts containing all the data on each person in the dataset
data_dict = pickle.load( open("../final_project/final_project_dataset.pkl", "r") )
### there's an outlier--remove it! 
data_dict.pop("TOTAL", 0)

'''
print data_dict
minVal = 10000000000000000000000000
maxVal = -1
for key in data_dict:
	if data_dict[key]['salary'] != 'NaN':
		minVal = min(minVal, data_dict[key]['salary'])
		maxVal = max(maxVal, data_dict[key]['salary'])

print 'MIN :', minVal
print 'Max :', maxVal
'''

### the input features we want to use 
### can be any key in the person-level dictionary (salary, director_fees, etc.) 
feature_1 = "salary"

#feature_2 = "exercised_stock_options"
feature_2 = "from_messages"
poi  = "poi"
features_list = [poi, feature_1, feature_2]
data = featureFormat(data_dict, features_list )
poi, finance_features = targetFeatureSplit( data )

from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()
finance_features = scaler.fit_transform(finance_features)


'''
salary = numpy.array(finance_features)
salary = salary[:, 0]
salary = numpy.append(salary, 200000)
print salary.reshape(len(salary), 1)

print('===============================================')
scaler = MinMaxScaler()
transformed = scaler.fit_transform(salary.reshape(len(salary), 1))
print transformed[len(transformed)-1, 0]

stock = numpy.array(finance_features)
stock = stock[:, 1]
stock = numpy.append(stock, 1000000)
scaler = MinMaxScaler()
transformed = scaler.fit_transform(stock.reshape(len(stock), 1))
print transformed[len(transformed)-1, 0]
'''

### in the "clustering with 3 features" part of the mini-project,
### you'll want to change this line to 
### for f1, f2, _ in finance_features:
### (as it's currently written, the line below assumes 2 features)
for f1, f2 in finance_features:
    plt.scatter( f1, f2 )
    #numpy.append(salary, numpy.array([f1]))
 
#print salary
plt.show()


### cluster here; create predictions of the cluster labels
### for the data and store them to a list called pred
from sklearn.cluster import KMeans

kmeans = KMeans(n_clusters = 2).fit(finance_features)
pred = kmeans.predict(finance_features)


### rename the "name" parameter when you change the number of features
### so that the figure gets saved to a different file
try:
    Draw(pred, finance_features, poi, mark_poi=False, name="clusters.pdf", f1_name=feature_1, f2_name=feature_2)
except NameError:
    print "no predictions object named pred found, no clusters to plot"
