#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 3 (decision tree) mini-project.

    Use a Decision Tree to identify emails from the Enron corpus by author:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess


### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()




#########################################################
### your code goes here ###
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score

print 'Number of features', len(features_train[0])

t0 = time()
clf = DecisionTreeClassifier(min_samples_split=40)
clf.fit(features_train, labels_train)
print 'Learning time', round(time()-t0, 3)

t1 = time()
predicts = clf.predict(features_test)
print 'Predicting time', round(time()-t1, 3)

print 'Accuracy %.2f' %accuracy_score(labels_test, predicts) 


'''-------------------------logs-------------------------

min_samples_split = 50
training datasets= 100%
Number of features 3785
Learning time 120.853
Predicting time 0.55
Accuracy 0.98


min_samples_split = 50
training datasets= 100%
Number of features 379
Learning time 7.456
Predicting time 0.003
Accuracy 0.97


min_samples_split = 40
training datasets= 100%
Number of features 3785
Learning time 105.112
Predicting time 4.257
Accuracy 0.98

'''
