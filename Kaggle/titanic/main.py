import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier

# Extracting important data from train.csv
features_field = ["Survived", "Pclass", "Sex", "SibSp",  "Fare"]
data = pd.read_csv('train.csv', usecols = features_field)


# Dividing features and labels
features = data.iloc[:, [1,2,3,4]].values
labels = data.iloc[:, 0].values

# Converting string into integers
#features = np.where(features[, 1] == 'female', 1, 0)
for i, row in enumerate(features):
	if row[1] == 'female':
		features[i][1] = 1
	else:
		features[i][1] = 0


# Splitting training and testing data (20% test data)
features_train, features_test, labels_train, labels_test = train_test_split(features, labels, test_size=.10, random_state=42)


# Building Classifier, Accuracy testing
clf = DecisionTreeClassifier() 										# Score on Kaggle = 0.75598
#clf = RandomForestClassifier()
clf.fit(features_train, labels_train)
pred_test = clf.predict(features_test)
pred_train = clf.predict(features_train)

print('Accuracy on train', accuracy_score(labels_train, pred_train))
print('Accuracy on test', accuracy_score(labels_test, pred_test))
print('Feature Importance : ', clf.feature_importances_)





'''



# Importing the test.csv
features_field[0] = "PassengerId"
test_data = pd.read_csv('test.csv', usecols = features_field)

test_features = test_data.iloc[:, [1,2,3,4]].values
passengerId = test_data.iloc[:, 0].values

for i, row in enumerate(test_features):
	if row[1] == 'female':
		test_features[i][1] = 1
	else:
		test_features[i][1] = 0
	test_features[i][3] = float(row[3])
	print(row)
	for j, x in enumerate(row):
		if str(x) == 'nan':
			test_features[i][j] = 10
#print(test_features)

test_pred = clf.predict(test_features)

test_data["Survived"] = pd.Series(test_pred)
test_data.to_csv('solved.csv')


'''
