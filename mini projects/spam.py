import re

words = dict()

with open('test.txt', 'r') as f:
	for line in f:
		for word in re.findall(r'\w+', line):
			if word in words:
				words[word]+=1
			else:
				words[word] = 1

print("total unique words found", len(words))
for key in words:
	print('word = %s, count = %.2f' %(key, words[key]/len(words)))

